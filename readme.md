# Scary Stories!

### A game for [\#sleepoverjam](http://jams.gamejolt.io/sleepoverjam/developers) 2017

## What is it?

You are hosting a sleepover for your three best friends. So of course you decide to tell each other scary stories at midnight!

## How to play

Become the master of scary stories by scaring your friends' socks off! Compose spooky stories that will give your friends lasting chills. If you fill up any one of your friends' fear meters, they will flee the story circle in terror.

Stories have four main elements to them: a setting, a protagonist, the dangerous element, and a resolution. Choose your story elements wisely to create tales with a long-lasting spook factor. Well-chosen story elements synergize with other elements of the story and create extra terror. But don't take too long composing a story. Your friends' fear will gradually decrease over time. If everyone's chill level drops to zero, your game ends.

## Set-up and build

To run the game in develpoment mode on your machine, make sure you have node installed.<sup>*</sup>

`npm install`

`npm serve` will start a webpack-dev-server on your localhost:9000.

<sub>*The project contains an `.nvmrc` file as well if you use `nvm` to manage your node versions (you should!).</sub>

