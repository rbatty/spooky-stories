let timer = {};
function startTick(callback) {
	timer = setInterval(callback, 250);
}
function stopTick(){
	clearInterval(timer)
}

export default {
	startTick: startTick,
	stopTick: stopTick
}