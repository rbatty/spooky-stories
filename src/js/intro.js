import UI from './ui';
import Game from './main';

let container;

function init(){
	container = UI.getContainer();
	container.innerHTML = intro;
	createStartButton();
}

function createStartButton(){
	var button = document.createElement("button");
	button.innerHTML = "Play now";
	button.onclick = function(e){
		Game.updateState("friends");
	}
	var article = document.getElementsByTagName("article")[0];
	article.appendChild(button);
}

export default {
	init: init
}

var intro = `<article class=\"intro\"><h1>Scary Stories!</h1>
	<h2>Created for #sleepoverjam 2017<h2>
	<p>Become the master of scary stories by scaring your friends' socks off! 
	Compose spooky stories that will give your friends lasting chills. If you fill up any one of your friends' 
	fear meters, they will flee the story circle in terror.</p>
	<p>Stories have four main elements to them: a setting, a protagonist, the dangerous element, and a resolution. 
	Choose your story elements wisely to create tales with a long-lasting spook factor. Well-chosen story elements 
	synergize with other elements of the story and create extra terror. But don't take too long composing a story. 
	Your friends' fear will gradually decrease over time. If everyone's chill level drops to zero, your game ends.</p>
	</article>`;

