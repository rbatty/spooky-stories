import Game from './main';
import UI from './ui';
import StorySelections from './story_selections';

import friendsData from '../data/friends';
import settingData from '../data/settings';
import protagonistData from '../data/protagonists';
import dangerData from '../data/danger';
import resolutionData from '../data/resolution';

let protagonists, settings, dangers, resolutions;

var storySelections = new StorySelections()
storySelections.initialize();

function buildButtons(type, dataArray, tab){
	var label = document.createElement("p");
	label.innerHTML = type;
	tab.appendChild(label);
	
	var buttonContainer = document.createElement("div");
	buttonContainer.classList.add("choice-container");
	var buttons = [];
	for (var i = dataArray.length-1; i >= 0; i-- ){
		var button = document.createElement('button');
		button.classList.add(type+'-button');
		button.innerHTML = dataArray[i].label;
		button.onclick = function(e){
			if(e.target.classList.contains('active')) {
				return;
			} else {
				var elementClass = e.target.classList[0];
				var allButtons = document.getElementsByClassName(elementClass);
				for (var j = allButtons.length-1; j >= 0; j--){
					allButtons[j].classList.remove('active');
				}
				storySelections.data[type] = e.target.innerHTML;
				e.target.classList.add('active');
			}
		}
		buttons.push(button);
		buttonContainer.appendChild(button);
	}
	tab.appendChild(buttonContainer);
	//return buttons;
}

function createUI(){
	var container = UI.getContainer();
	container.classList.add('tab-container');
	var settingTab = document.createElement('section')
	var protagonistTab = document.createElement('section');
	var dangerTab = document.createElement('section');
	var resolutionTab = document.createElement('section');
	container.appendChild(settingTab);
	container.appendChild(protagonistTab);
	container.appendChild(dangerTab);
	container.appendChild(resolutionTab);

	protagonists = protagonistData.data;
	settings = settingData.data;
	dangers = dangerData.data;
	resolutions = resolutionData.data;

	buildButtons('protagonists', protagonists, protagonistTab);
	buildButtons('settings', settings, settingTab);
	buildButtons('dangers', dangers, dangerTab);
	buildButtons('resolutions', resolutions, resolutionTab);

	var submitButton = document.createElement('button');
	submitButton.innerHTML = "Tell this story!";
	submitButton.onclick = submitStory;
	container.appendChild(submitButton);
}

function submitStory(e){
	if (storySelections.validate()) {
		//remove any existing errors on successful submission
		UI.dismissError("submission-error");
		//tell the story
		evaluateStory();
	} else {
		UI.displayError("You need to choose all four story elements.","submission-error");
	}	
}

function evaluateStory(){
	var synergy = [];
	var story = {};
	for (var key in storySelections.data) {
		var element = {};
		switch (key){
			case "protagonists":
				element = _.find(protagonists, {"label":storySelections.data[key]});
				console.log(element)
				story.protagonist = element.label;
				break
			case "settings":
				element = _.find(settings, {"label":storySelections.data[key]});
				story.setting = element.label;
				break
			case "dangers":
				element = _.find(dangers, {"label":storySelections.data[key]});
				story.danger = element.label;
				break
			case "resolutions":
				element = _.find(resolutions, {"label":storySelections.data[key]});
				story.resolution = element.label;
				break
		}

		if (element.categories) {
			for (var i = element.categories.length-1; i >= 0; i--){
				synergy.push(element.categories[i])
			}
		}
	}

	//count the synergies to assign bonuses
	var synergies = _(synergy).countBy().pickBy(function(value, key) {return value > 1}).value()

	var completedStory = `You tell the terrifying tale of the encounter in <b>${story.setting}</b> 
	where the <b>${story.protagonist}</b> overcomes <b>${story.danger}</b> with <b>${story.resolution}</b>!`;

	Game.updateStory(completedStory, synergies);
	Game.updateState('telltale')

}

export default {
	createUI: createUI
}