import Timer from './timer';
import UI from './ui';

import Intro from './intro';
import Composition from './composition';
import Friends from './friends';
import TellTale from './telltale';
import End from './end';

export default {
	intro: {
  	enter: function(){
  		UI.clearDisplay();
  		Intro.init();
  	},
    exit: function(){}
  },
  friends: {
  	enter: function(){
  		UI.clearDisplay();
  		Friends.init();
  		Timer.startTick(Friends.update);
  	},
    exit: function(){
    	Timer.stopTick();
    },
    handleInput: function(){},
    update: function(){},
  },
  compose: {
  	enter: function(){
  		UI.clearDisplay();
  		Composition.createUI();
  		Timer.startTick(Friends.update);
  	},
    exit: function(){
    	Timer.stopTick();
    },
    handleInput: function(){},
    update: function(){},
  },
  telltale: {
  	enter: function(){
  		UI.clearDisplay();
  		TellTale.init();
  	},
  	exit: function(){
    	//Timer.stopTick();
    },
  },
  win: {
  	enter: function(){
  		End.win();
  	},
    exit: function(){},
  },
  lose: {
  	enter: function(){
  		End.lose();
  	},
    exit: function(){},
  }
}