let main, container, wrapper, output;

function init(){
	main = document.createElement('main');

	container = document.createElement('div');
	container.classList.add("container");

	wrapper = document.createElement('div');
	wrapper.id = "game-wrapper";
	wrapper.classList.add("wrapper");

	output = document.createElement('div');
	output.id = "message-console";
	output.classList.add("console");

	container.appendChild(wrapper);
	container.appendChild(output);

	main.appendChild(container);
	document.body.appendChild(main);
}

function getContainer(){
	return wrapper;
}

function clearDisplay(){
	wrapper.innerHTML = "";
}

function displayError(message, type){
	var existingError = document.getElementById(type);
	if (existingError) {
		existingError.innerHTML = message;
	} else {
		var errorMessage = document.createElement('div');
		errorMessage.id = type;
		errorMessage.classList.add('error');
		errorMessage.innerHTML = message;
		document.body.appendChild(errorMessage);
	}
}

function dismissError(type){
	var error = document.getElementById(type);
	if (error) {
		document.body.removeChild(error);
	}
}

function notifyConsole(message){
	var node = document.createElement('p');
	node.innerHTML = message;
	output.appendChild(node);
}

export default {
	clearDisplay: clearDisplay,
	init: init,
	getContainer: getContainer,
	displayError: displayError,
	dismissError: dismissError,
	notifyConsole: notifyConsole
}