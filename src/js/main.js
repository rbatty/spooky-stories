import _ from 'lodash';
import Timer from './timer';
import States from './states';
import UI from './ui';

import Friends from '../data/friends';

let state, story, synergies;
let firstStoryBegun = false;
let friends = [];

//randomly chosen friends for the round
function chooseFriends(){
	var friendsData = Friends.data;
	var i = 3;
	while (i > 0){
		var random = (Math.ceil(Math.random()*friendsData.length))-1;
		friendsData[random].terror = 0;
		friends.push(friendsData[random]);
		friendsData.splice(random,1);
		i--;
	}
}

function getFriends(){
	return friends;
}

function updateState(newState){
	if (state) {
		state.exit();
	}
	state = States[newState];
	state.enter();
}

function updateStoryBegun(bool){
	firstStoryBegun = bool;
}

function getStoryBegun(){
	return firstStoryBegun;
}

function updateStory(newStory, newSynergies){
	story = newStory;
	synergies = newSynergies;
}

function getStory(){
	return story;
}

function getSynergies(){
	return synergies;
}

export default {
	getFriends: getFriends,
	getSynergies: getSynergies,
	getStory: getStory,
	getStoryBegun: getStoryBegun,
	updateStoryBegun: updateStoryBegun,
	updateState: updateState,
	updateStory: updateStory,
}

/* Start Game */
UI.init();
chooseFriends();

updateState("intro");
