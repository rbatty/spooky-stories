import States from './states';
import Game from './main';
import UI from './ui';

let friends, container, friendsHolder, hasBegun;

function update(){
	if (!hasBegun) {
		hasBegun = Game.getStoryBegun();
	}
	if (hasBegun) {
		//start Fear drain
		reduceFear();
		evaluate();
	}
	draw();
}

function draw(){
	var elements = document.getElementsByClassName('friend-block');
	for (var i=friends.length-1; i >= 0; i--){
		for (var j=elements.length-1; j >= 0; j--) {
			if (elements[j].id === friends[i].name){
				var text = elements[j].getElementsByTagName('p')[0];
				text.innerHTML = "<p>" + friends[i].terror + " fear</p>";
				var meter = elements[j].getElementsByClassName('terror-meter')[0];
				var svg = meter.getElementsByClassName("terror-bar")[0];
				svg.setAttribute('height', friends[i].terror);
			}
		}
	}
}

function reduceFear(){
	var rate = 0.07;
	for (var i=friends.length-1; i >= 0; i--){
		if (friends[i].terror > 0) {
			var newTerror = friends[i].terror - rate;
			friends[i].terror = Math.ceil(newTerror * 100) / 100;
		}
		if (friends[i].terror < 0) {
			friends[i].terror = 0;
		}
	}
}

function evaluate(){
	var failCount = 0;
	for (var i = friends.length-1; i >= 0; i--){
		if (friends[i].terror >= 100) {
			Game.updateState("win");
			return;
		} else if (friends[i].terror <= 0) {
			failCount++;
		}
	}

	if (failCount === friends.length){
		Game.updateState("lose");
	}
}

function init(){
	hasBegun = Game.getStoryBegun();
	friends = Game.getFriends();
	console.log(friends)
	container = UI.getContainer();
	drawComposeButton();
	drawFriends();
}

function drawFriends(){
	friendsHolder = document.createElement('div');
	friendsHolder.classList.add('friends-wrapper');
	for (var i=friends.length-1; i >= 0; i--){
		var element = document.createElement('div');
		element.id = friends[i].name;
		element.classList.add("friend-block");
		element.innerHTML = friends[i].name;
		element.innerHTML += "<p>" + friends[i].terror + " fear</p>";
		var terrorMeter = document.createElement('div');
		terrorMeter.classList.add('terror-meter');
		var terrorBar = drawSvg();
		//terrorBar.classList.add("terror-bar");
		terrorMeter.appendChild(terrorBar);

		element.appendChild(terrorMeter);
		friendsHolder.appendChild(element);
	}
	container.appendChild(friendsHolder);
}

function drawSvg(){
	var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
	svg.setAttribute('width', 30);
  svg.setAttribute('height', 100);
  svg.setAttribute("viewBox", "0 0 30 100"); 

  var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
  rect.setAttribute('class', 'terror-bar');
  rect.setAttribute('x', 0);
  rect.setAttribute('y', 0);
  rect.setAttribute('height', '0');
  rect.setAttribute('width', '40');
  rect.setAttribute('fill', '#adff2f');

  svg.appendChild(rect);
	return svg;
}

function drawComposeButton(){
	var button = document.createElement("button");
	button.innerHTML = "compose story";
	button.onclick = function(e){
		Game.updateState("compose");
	}
	container.appendChild(button);
}

export default {
	init: init,
	update: update
}