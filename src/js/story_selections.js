var StorySelections = function(){
	this.data = {};
}

StorySelections.prototype = {
	initialize: function(){
		this.data = {
			'protagonists': '',
			'settings': '',
			'dangers': '',
			'resolutions': ''
		}
	},
	validate: function(){
		console.log('validating')
		for (var key in this.data) {
			if (!this.data[key]) {
				//can't submit because a choice is missing
				return false;
			}
		}
		return true;	
	}
}

export default StorySelections;