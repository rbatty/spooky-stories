import Game from './main';
import UI from './ui';

let container;

function init(){
	container = UI.getContainer();
	var completedStory = Game.getStory();
	UI.displayError(completedStory, "story");
	updateFear();
	drawNextButton();
}

function updateFear() {
	Game.updateStoryBegun(true);
	var synergies = Game.getSynergies();
	var friends = Game.getFriends();
	for (var i = friends.length-1; i >= 0; i--){
		for (var key in synergies) {
			if (friends[i].fears[0] === key) {
				UI.notifyConsole(friends[i].name + " is really scared!");
				var bonus = 5 * synergies[key];
				friends[i].terror += bonus;
			}
		}
		friends[i].terror += 5;
	}
}

function drawNextButton(){
	var button = document.createElement("button");
	button.innerHTML = "all right!";
	button.onclick = function(e){
		Game.updateState("friends");
	}
	container.appendChild(button);
}

export default {
	init:init
}