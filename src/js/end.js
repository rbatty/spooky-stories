import UI from './ui';

function win(){
	UI.clearDisplay();
	UI.notifyConsole("YOU WIN!");
}
function lose(){
	UI.clearDisplay();
	UI.notifyConsole("YOU LOSE!");
}

export default {
	win: win,
	lose: lose	
}