var webpack = require('webpack');
var path = require('path');

var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
	entry: [
		'./src/js/main.js',
		'./src/sass/main.scss'
	],
	output: {
		path: path.resolve(__dirname, './dist'),
		filename: 'bundle.js',
		publicPath: '/dist/'
	},
	devServer: {
		port: '9000'
	},
	module: {
		rules: [
			{
				test: /\.scss$/,
				use: ExtractTextPlugin.extract({
					use: ['css-loader', 'sass-loader'],
					fallback: 'style-loader'
				}),
				include: path.resolve(__dirname, './src/sass')
			},
			{
				test: /\.js$/,
				exclude: path.resolve(__dirname, './node_modules'),
				loader: 'babel-loader'
			}
		]
	},
	plugins: [
		new ExtractTextPlugin('styles.css')
	]
}
